package com.example.admin.bluetoothapp.RegisteredUsers

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.admin.bluetoothapp.R
import kotlinx.android.synthetic.main.user_list_adapter.view.*
import java.lang.Long

class RegisterUserAdapter(private val userList:ArrayList<String>,
                          private val context: Context):RecyclerView.Adapter<RegisterUserAdapter.RegisteredUserView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegisteredUserView {
        val viewHolder = LayoutInflater.from(parent.context).inflate(R.layout.user_list_adapter, parent,
                false)
        return  RegisteredUserView(viewHolder)
    }

    override fun getItemCount(): Int {
       return userList.size
    }

    override fun onBindViewHolder(holder: RegisteredUserView, position: Int) {
        val userRole = userList[position].substring(0,2)
        val mobileNo = userList[position].substring(2)
        Log.e("TAG","User Role "+userRole)
        Log.e("TAG","Mobile No "+mobileNo)
         holder.userMob.text = Long.parseLong(mobileNo,16).toString()
                 //Integer.parseInt(mobileNo,16).toString()
         when(userRole)
         {
             "RM"->{
                 holder.userRole.text = "Master"
                 holder.userRole.background =(context.getDrawable(R.drawable.rectangular_editext_white))


             }

           /*  "RU" ->{
                 holder.userRole.text = "User"
             }*/
         }

    }


    class RegisteredUserView(item:View):RecyclerView.ViewHolder(item)
    {
        val userMob = item.userMobNo
        val userRole = item.userRole
    }
}