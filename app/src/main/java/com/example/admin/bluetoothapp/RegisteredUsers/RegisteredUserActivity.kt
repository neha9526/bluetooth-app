package com.example.admin.bluetoothapp.RegisteredUsers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import com.example.admin.bluetoothapp.R
import kotlinx.android.synthetic.main.activity_registered_user.*

class RegisteredUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registered_user)

        //toolbar setup
        val baseToolbar: Toolbar = findViewById(R.id.base_toolbar)
        setSupportActionBar(baseToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        baseToolbar.setNavigationOnClickListener {
            finish()
        }
        //recyclerview setup
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true
        userList.layoutManager = linearLayoutManager
        // logDetailsList.relayo
        //receiving data through intent
        val intent = intent
        val receivedData:ArrayList<String> =  intent.extras.getStringArrayList("Users")

        //set adapter
        if(receivedData.isNotEmpty())
        {
            userList.adapter = RegisterUserAdapter(receivedData,this)
        }



    }
}
