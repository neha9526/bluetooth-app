package com.example.admin.bluetoothapp.pojo

import android.os.Parcel
import android.os.Parcelable

class DoorStatusPojo(var lockStatus:String, val lockNumber:String, val lockName:String):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
       parcel.writeString(lockStatus)
        parcel.writeString(lockNumber)
        parcel.writeString(lockName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DoorStatusPojo> {
        override fun createFromParcel(parcel: Parcel): DoorStatusPojo {
            return DoorStatusPojo(parcel)
        }

        override fun newArray(size: Int): Array<DoorStatusPojo?> {
            return arrayOfNulls(size)
        }
    }

}