package com.example.admin.bluetoothapp

import java.util.*

class GATTConstants {
    object Constants{
        //bluetooth state
        private val STATE_DISCONNECTED = 0
        private val STATE_CONNECTING = 1
        private val STATE_CONNECTED = 2

        //lock status
        const val LOCK_ST_LOCKED = 1
        const val LOCK_ST_UNLOCKED = 0

        //intent actins
        val ACTION_DEVICE_FOUND = "com.example.admin.bluetoothapp.ACTION_DEVICE_FOUND"
        val ACTION_GATT_CONNECTED = "com.example.admin.bluetoothapp.ACTION_GATT_CONNECTED"
        val ACTION_GATT_DISCONNECTED = "com.example.admin.bluetoothapp.ACTION_GATT_DISCONNECTED"
        val ACTION_GATT_SERVICES_DISCOVERED = "com.example.admin.bluetoothapp.ACTION_GATT_SERVICES_DISCOVERED"
        val ACTION_DATA_AVAILABLE = "com.example.admin.bluetoothapp.ACTION_DATA_AVAILABLE"
        val ACTION_DATA_WRITE = "com.example.admin.bluetoothapp.ACTION_DATA_WRITE"
        val EXTRA_DATA = "com.example.admin.bluetoothapp.EXTRA_DATA"
        val DEVICE_LIST = "com.example.admin.bluetoothapp.DEVICE_LIST"
        val DEVICE_DOES_NOT_SUPPORT_UART = "com.example.admin.bluetoothapp.DEVICE_DOES_NOT_SUPPORT_UART"
        val ACTION_RESTART_SCAN = "com.example.admin.bluetoothapp.ACTION_RESTART_SCAN"

        //UUIDs
        val TX_CHAR_UUID = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E")!!
        val RX_CHAR_UUID = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E")
        val RX_SERVICE_UUID = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
        val CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

    }


}