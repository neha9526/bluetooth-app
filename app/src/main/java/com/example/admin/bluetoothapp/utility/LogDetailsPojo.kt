package com.example.admin.bluetoothapp.utility

import android.os.Parcel
import android.os.Parcelable

class LogDetailsPojo(val timeList:Long,
                     val mobileList:Long,
                     val latchName:String,
                     val action:Int) :Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(timeList)
        parcel.writeLong(mobileList)
        parcel.writeString(latchName)
        parcel.writeInt(action)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LogDetailsPojo> {
        override fun createFromParcel(parcel: Parcel): LogDetailsPojo {
            return LogDetailsPojo(parcel)
        }

        override fun newArray(size: Int): Array<LogDetailsPojo?> {
            return arrayOfNulls(size)
        }
    }
}