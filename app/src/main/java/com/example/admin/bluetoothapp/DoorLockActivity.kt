package com.example.admin.bluetoothapp

import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.example.admin.bluetoothapp.RegisteredUsers.UserListPojo
import com.example.admin.bluetoothapp.controllers.MainController
import com.example.admin.bluetoothapp.pojo.DoorListPojo
import com.example.admin.bluetoothapp.pojo.DoorStatusPojo
import com.example.admin.bluetoothapp.utility.*
import kotlinx.android.synthetic.main.activity_door_lock.*
import kotlinx.android.synthetic.main.layout_door_controller.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import kotlin.collections.ArrayList


class DoorLockActivity : BaseAppcompactAcivity(), BroadcastInterface {

    override val layoutIdForInjection: Int = R.layout.activity_door_lock
    override val broadcastInterface: BroadcastInterface = this

    private var dataFrameList: ArrayList<ByteArray>? = null
    private var index: Int = 0
    private var timeLong: Long? = null
    private var mobileNoLong: Long? = null
    private var latchName: String = ""
    private var actionInt: Int? = null
    private var logList: ArrayList<LogDetailsPojo>? = null
    var lockList: ArrayList<DoorStatusPojo>? = null
    var userList: ArrayList<String>? = null

    override fun onBleConnected(mBluetoothDevice: BluetoothDevice) {
        mState = BLE_PROFILE_CONNECTED
        Toast.makeText(this, "Ble Connected", Toast.LENGTH_SHORT).show()
    }

    override fun onBleDisconnected(mBluetoothDevice: BluetoothDevice) {
        mState = BLE_PROFILE_DISCONNECTED
        Toast.makeText(this, "Ble Disconnected", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onServiceDiscovered(action: String) {
        if (action == GATTConstants.Constants.ACTION_GATT_SERVICES_DISCOVERED) {
            mService!!.enableTXNotification()
        }
    }

    override fun onDataAvailable(data: String) {
        Log.e("TAG", "Received data " + data)
        when {
            data[0] == 'T' -> {
                if (data.length > 1) {
                    timeLong = data.substring(1, data.length - 1).toLong(16)
                    Log.e("TAG","Time long "+timeLong)
                    actionInt = data.substring(data.length - 1).toInt()
                }
            }
            data[0] == 'L' -> {
                if (data.length > 1) {
                    latchName = data.substring(1)
                }
            }
            data[0] == 'M' -> {

                if (data.length > 1) {
                    when (data) {
                        "M FULL" -> {
                            EventBus.getDefault().post(AllResponseEvent(data, mService!!))
                        }
                        else -> {
                            mobileNoLong = data.substring(1).toLong(16)
                            logList!!.add(LogDetailsPojo(timeLong!!, mobileNoLong!!, latchName, actionInt!!))
                        }
                    }
                }
            }
            data == "ENDL" -> {
                EventBus.getDefault().post(LogDetailsEvent(logList!!))
            }
            data =="ENDU"->{
                EventBus.getDefault().post(UserListPojo(userList!!))
            }
            data.contains("RM") || data.contains("RU") -> {
                userList!!.add(data)
            }
            data =="ERROR NIGHT ACCESS"->{
                Toast.makeText(this,"Night access is not allowed",Toast.LENGTH_SHORT).show()
            }
            data =="ERROR DAY ACCESS"->{
                Toast.makeText(this,"Day access is not allowed",Toast.LENGTH_SHORT).show()
            }
            else -> {
                EventBus.getDefault().post(AllResponseEvent(data, mService!!))
            }
        }
    }

    override fun onDataWrite() {
        Log.e("TAG", "Successful data transmit")
        index++
        sendData(dataFrameList!!)
    }

    override fun onResume() {
        super.onResume()
        logList = ArrayList()
        userList = ArrayList()
    }

    override fun uartNotSupported(action: String) {
        if (action == GATTConstants.Constants.DEVICE_DOES_NOT_SUPPORT_UART) {
            Toast.makeText(this@DoorLockActivity, getString(R.string.UART_not_supported), Toast.LENGTH_SHORT)
                    .show()
            mService!!.disconnect()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //receive data through intent
        lockList = ArrayList()
        userList = ArrayList()
        lockList = intent.getParcelableArrayListExtra<DoorStatusPojo>(getString(R.string.ble_data))

        if (lockList != null) {
            EventBus.getDefault().postSticky(DoorListPojo(lockList!!))
        }

        mState = intent.getIntExtra("BLE_STATE", 0)
        //Preferences.setAuthData(this,response)
        dataFrameList = ArrayList()
        logList = ArrayList()


        //toolbar setup
        val baseToolbar: Toolbar = findViewById(R.id.base_toolbar)
        setSupportActionBar(baseToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        baseToolbar.setNavigationOnClickListener {
            onBackpressEvent()
            finish()
        }

        //setting up root controller
        val frameLayout: ViewGroup = frameLayout
        val router: Router = Conductor.attachRouter(this, frameLayout, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(MainController()))
        }

    }

    override fun onStart() {
        super.onStart()


        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    //listening write data event
    @Subscribe
    fun performServiceOperations(writeDataEvent: WriteDataEvent) {
        logList = ArrayList()
        index = 0
        dataFrameList = writeDataEvent.dataFrameList
        if (writeDataEvent.dataFrameList.size > 1) {

            sendData(dataFrameList!!)
        } else {
            mService!!.writeRXCharacteristic(dataFrameList!![index])
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    //sending data
    private fun sendData(dataList: ArrayList<ByteArray>) {
        Timer().schedule(object : TimerTask() {
            override fun run() {
                if (index < dataList.size) {
                    mService!!.writeRXCharacteristic(dataList[index])
                }
            }
        }, 300)
    }

    //Subscribe
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED, sticky = true)
    fun receiveConnectionData(authSuccessEvent: AuthSuccessEvent) {
        mService = authSuccessEvent.mService
        mBluetoothAdapter = authSuccessEvent.mBluetoothAdapter

    }


}
