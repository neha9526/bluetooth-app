package com.example.admin.bluetoothapp

import android.bluetooth.BluetoothDevice

interface BroadcastInterface {

    fun onBleConnected(mBluetoothDevice: BluetoothDevice)
    fun onBleDisconnected(mBluetoothDevice: BluetoothDevice)
    fun onServiceDiscovered(action: String)
    fun onDataAvailable(data: String)
    fun onDataWrite()
    fun uartNotSupported(action: String)


}