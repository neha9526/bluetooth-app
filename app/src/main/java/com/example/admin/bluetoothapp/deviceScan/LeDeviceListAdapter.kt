package com.example.admin.bluetoothapp.deviceScan

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.admin.bluetoothapp.R


class LeDeviceListAdapter(private val context: Context,private var mLeDevices: ArrayList<BluetoothDevice>?) :BaseAdapter() {

    private var mLayoutInflater:LayoutInflater? = null
  //  private var mLeDevices: ArrayList<BluetoothDevice>? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var viewHolder: ViewHolder
        // General ListView optimization code.
        mLayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var adapterView:View? = null
        viewHolder = ViewHolder()
        if (adapterView == null) {
            adapterView = mLayoutInflater!!.inflate(R.layout.adapter_device_list,parent,false)
            viewHolder.deviceAddress = adapterView.findViewById(R.id.deviceAddress)
            viewHolder.deviceName = adapterView.findViewById(R.id.deviceName)
            viewHolder.listItem = adapterView.findViewById(R.id.listItem)
            adapterView!!.tag = viewHolder
        } else {
            viewHolder = adapterView.tag as ViewHolder
        }

        val deviceName =   mLeDevices!![position].name
        if (deviceName != null)
        {
            viewHolder.deviceName!!.text = deviceName.substring(context.getString(R.string.uli_id).length+1)
            viewHolder.deviceAddress!!.text = mLeDevices!![position].address
        }
        else
        {
            viewHolder.deviceName!!.setText(R.string.unknown_device)
            viewHolder.deviceAddress!!.text = mLeDevices!![position].address
        }

        return adapterView
    }

    override fun getItem(position: Int): Any {
        return mLeDevices!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return  mLeDevices!!.size
    }

    fun addDevice(device: BluetoothDevice) {
        if (!mLeDevices!!.contains(device)) {
            mLeDevices!!.add(device)
        }
    }
    fun getDevice(position: Int): BluetoothDevice {
        return mLeDevices!![position]
    }
     fun isListEmpty():Boolean
     {
         return mLeDevices!!.isEmpty()
     }

    fun clear() {
        mLeDevices!!.clear()
    }

    internal class ViewHolder {
        var deviceName: TextView? = null
        var deviceAddress: TextView? = null
        var listItem:RelativeLayout? = null
    }
}