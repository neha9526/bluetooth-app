package com.example.admin.bluetoothapp.controllers

import android.support.design.widget.TabItem
import android.support.design.widget.TabLayout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.support.RouterPagerAdapter
import com.example.admin.bluetoothapp.R
import com.example.admin.bluetoothapp.pojo.DoorListPojo
import com.example.admin.bluetoothapp.pojo.DoorStatusPojo
import com.example.admin.bluetoothapp.utility.Preferences
import kotlinx.android.synthetic.main.layout_main_controller.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


 open class MainController():Controller(){

     private  var routerPagerAdapter: RouterPagerAdapter?=null
    private  var controllerMap: MutableMap<Int, Controller>?=null
    private var userTab:TabItem? = null
    private var selectedController:Controller? =null
    var doorList:ArrayList<DoorStatusPojo>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        return inflater.inflate(R.layout.layout_main_controller, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        userTab = view.findViewById(R.id.userTab)
        Log.e("TAG","Inside on Attach")

        if(controllerMap==null && routerPagerAdapter==null){
            controllerMap = HashMap()
            setUpControllers(doorList!!,view)

            routerPagerAdapter = object : RouterPagerAdapter(this) {
                override fun configureRouter(router: Router, position: Int) {
                    if(!router.hasRootController())
                    {
                        //passing argument
                      args.putParcelableArrayList("DoorList",doorList)
                        router.pushController(RouterTransaction
                                .with(controllerMap!![position]!!)
                                .pushChangeHandler(FadeChangeHandler())
                                .popChangeHandler(FadeChangeHandler()))
                    }
                }

                override fun getCount(): Int {
                    return controllerMap!!.size
                }
            }


            view.tabPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(view.tabLayout))
            view.tabPager.adapter = routerPagerAdapter

        }

        //tab click listener
        view.tabLayout.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                view.tabPager.currentItem = tab.position
                selectedController = controllerMap!![tab.position]
                Log.e("TAG","Controller "+selectedController)
            }


            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }



   //set up tabs
    private fun setUpControllers(doorList:ArrayList<DoorStatusPojo>,view:View)
    {
        var authResponse=""
        if(doorList.isNotEmpty())
        {
             authResponse = doorList[0].lockStatus
        }

        if(authResponse==applicationContext!!.getString(R.string.door_open_master) ||
                authResponse==applicationContext!!.getString(R.string.door_close_master))
        {
            controllerMap!![0] = LockController()
            controllerMap!![1] = UserController()
            controllerMap!![2] = SettingsController()

        }
        else if(authResponse== applicationContext!!.getString(R.string.door_open_normal) ||
                authResponse ==applicationContext!!.getString(R.string.door_close_normal)){
            view.tabLayout.removeTabAt(1)
            controllerMap!![0] = LockController()
            controllerMap!![1] = SettingsController()

        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN,sticky = true)
    fun receiveDoorList(doorListPojo: DoorListPojo)
    {
         doorList = ArrayList()
         doorList = doorListPojo.doorList

    }


    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }
}