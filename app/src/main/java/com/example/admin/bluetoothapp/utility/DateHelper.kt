package com.example.admin.bluetoothapp.utility

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by admin on 31-01-2018.
 */
object DateHelper {

    private val DATE_ONLY = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    private val TIME_ONLY = SimpleDateFormat("hh:mm", Locale.ENGLISH)
    private val DATA_DOC_TIME = SimpleDateFormat("dd-MMM-yy hh:mm:ss a")
    private val NEW_FORMAT = SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.ENGLISH)


    //date picker date conversion
    fun getYearMonthDateFormat(month: Int, year: Int, dateOfMonth: Int): String {
        val cal = Calendar.getInstance()
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.DAY_OF_MONTH, dateOfMonth)
        return DATE_ONLY.format(cal.time)
    }

    fun getHourMinSec(hour:Int,min:Int):String{
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR, hour)
        cal.set(Calendar.MINUTE, min)
        //cal.set(Calendar.SECOND, sec)
        return TIME_ONLY.format(cal.time)
    }

    fun getLongDate(date:String):Long{
      val date = NEW_FORMAT.parse(date)
        return date.time
    }

    fun getDateTime(millis: Long):String
    {

        val currentTime =Date(millis)
        return  DATA_DOC_TIME.format(currentTime)
    }
}