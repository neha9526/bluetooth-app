package com.example.admin.bluetoothapp.utility

import android.bluetooth.BluetoothAdapter
import com.example.admin.bluetoothapp.BleService

class AuthSuccessEvent(val mService:BleService,val mBluetoothAdapter: BluetoothAdapter) {}