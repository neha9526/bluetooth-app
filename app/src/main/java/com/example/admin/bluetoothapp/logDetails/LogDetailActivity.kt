package com.example.admin.bluetoothapp.logDetails

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.LinearLayout
import com.example.admin.bluetoothapp.ArchLifecycleApp
import com.example.admin.bluetoothapp.R
import com.example.admin.bluetoothapp.utility.LogDetailsPojo
import kotlinx.android.synthetic.main.activity_log_detail.*
import kotlinx.android.synthetic.main.activity_main.*


class LogDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_detail)

        //toolbar setup
        val baseToolbar: Toolbar = findViewById(R.id.base_toolbar)
        setSupportActionBar(baseToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        baseToolbar.setNavigationOnClickListener {
           finish()
        }
        //recyclerview setup
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true

        logDetailsList.layoutManager = linearLayoutManager
        //receiving data through intent
        val intent = intent
        val receivedData:ArrayList<LogDetailsPojo> =  intent.extras.getParcelableArrayList<LogDetailsPojo>("Logs")
       /* for(data in receivedData)
        {
            Log.e("TAG","Time "+data.timeList)
            Log.e("TAG","number "+data.mobileList)
            Log.e("TAG","action "+data.action)
        }*/
        //set adapter
        if(receivedData.isNotEmpty())
        {
            logDetailsList.adapter = LogAdapter(receivedData,this)
        }
    }

}
