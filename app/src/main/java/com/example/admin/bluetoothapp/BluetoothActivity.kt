package com.example.admin.bluetoothapp

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
import android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.Toolbar
import android.telephony.TelephonyManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.example.admin.bluetoothapp.GATTConstants.Constants.DEVICE_DOES_NOT_SUPPORT_UART
import com.example.admin.bluetoothapp.deviceScan.DeviceScanActivity
import com.example.admin.bluetoothapp.pojo.DoorStatusPojo
import com.example.admin.bluetoothapp.utility.AllResponseEvent
import com.example.admin.bluetoothapp.utility.AuthSuccessEvent
import com.example.admin.bluetoothapp.utility.DateHelper
import com.example.admin.bluetoothapp.utility.Preferences
import kotlinx.android.synthetic.main.activity_main.*
import org.greenrobot.eventbus.EventBus
import java.math.BigInteger
import java.nio.charset.Charset
import java.text.DateFormat
import java.util.*
import kotlin.collections.ArrayList


const val REQUEST_ENABLE_BT = 10
const val REQUEST_ID_MULTIPLE_PERMISSIONS = 100
const val REQUEST_SELECT_DEVICE = 1
const val BLE_PROFILE_CONNECTED = 20
const val BLE_PROFILE_DISCONNECTED = 21

class BluetoothActivity : BaseAppcompactAcivity(), BroadcastInterface {

    override fun onBleConnected(mBluetoothDevice: BluetoothDevice) {
        connectionData.visibility = View.VISIBLE
        val currentDateTimeString = DateFormat.getTimeInstance().format(Date())
        connectionStatus.text = "Connected to " + mBluetoothDevice!!.name.substring(getString(R.string.uli_id).length + 1)
        connectionTime.text = currentDateTimeString
        deviceConnect.text = getString(R.string.disconnect_ble)
        phoneData = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        //not to make verify user screen visible in Hassle free mode
    }

    override fun onBleDisconnected(mBluetoothDevice: BluetoothDevice) {
        hideProgress()
        connectionData.visibility = View.VISIBLE
        connectionStatus.text = "Disconnected " + mBluetoothDevice!!.name.substring(getString(R.string.uli_id).length + 1)
        deviceConnect.text = getString(R.string.bt_connect)
        verifyUser.visibility = View.GONE
        mService!!.mBluetoothGatt!!.close()

    }

    @SuppressLint("MissingPermission")
    override fun onServiceDiscovered(action: String) {
        Log.e("TAG", "Inside onServiceDiscovered")
        if (Preferences.getMobileNo(this).isEmpty()) {
            verifyUser.visibility = View.VISIBLE
        } else {
            showProgress()
            index = 0
            dataFrame = ArrayList()
            val mobComm = getString(R.string.hassel_free_comm) + Preferences.getMobileNo(this@BluetoothActivity)
            val imeiComm = getString(R.string.hassel_free_comm) + Preferences.getImeiNo(this@BluetoothActivity)
            var serialNo = getString(R.string.hassel_free_comm) + Preferences.getSerialNo(this@BluetoothActivity)
            if (serialNo.length > 19) {
                serialNo = serialNo.substring(0, 19)
            }

            dataFrame!!.add(mobComm.toByteArray(Charset.defaultCharset()))
            dataFrame!!.add(imeiComm.toByteArray(Charset.defaultCharset()))
            dataFrame!!.add(serialNo.toByteArray(Charset.defaultCharset()))
            sendData(dataFrame!!)
        }
    }

    override fun onDataAvailable(data: String) {
        hideProgress()
        Log.e("TAG", "On Data Available " + data)
        when {
            data == getString(R.string.error) -> {
                verifyUser.visibility = View.VISIBLE
                Toast.makeText(this@BluetoothActivity, "Invalid User", Toast.LENGTH_SHORT).show()
                EventBus.getDefault().post(AllResponseEvent(data, mService!!))
            }
            data.contains(getString(R.string.door_open_master)) || data.contains(getString(R.string.door_open_normal)) ||
                    data.contains(getString(R.string.door_close_normal)) || data.contains(getString(R.string.door_close_master)) -> {

                if (ArchLifecycleApp.Status.isActivityVisible()) {
                    val lockStatus = data.substring(0, 2)
                    val lockNo = data[2].toString()
                    val lockName = data.substring(data.lastIndexOf("_") + 1)
                    val doorListPojo = DoorStatusPojo(lockStatus, lockNo, lockName)

                    doorList!!.add(doorListPojo)
                }
                //Preferences.setLockName(this, mDevice!!.name.substring(getString(R.string.uli_id).length+1))
                //setting up shared pref for first time
                if (!mobileNoHex.isNullOrEmpty() && !imeiNoHex.isNullOrBlank() && !simSerialHex.isNullOrEmpty()) {
                    Preferences.setMobileNo(this@BluetoothActivity, mobileNoHex!!)
                    Preferences.setImeiNo(this@BluetoothActivity, imeiNoHex!!)
                    Preferences.setSerialNo(this@BluetoothActivity, simSerialHex!!)
                }
            }
            data == "END" -> {
                if (doorList!!.isNotEmpty()) {

                 /*   if (Preferences.getHassleFreeMode(this)) {
                        when (data) {
                            getString(R.string.door_close_normal), getString(R.string.door_close_master) -> {
                                val hexTimeStamp = System.currentTimeMillis().toString(16)
                                val hexCommand = getString(R.string.door_open_key) + hexTimeStamp
                                mService!!.writeRXCharacteristic(hexCommand.toByteArray(Charset.defaultCharset()))
                            }
                        }
                    }*/

                    val intent = Intent(this@BluetoothActivity, DoorLockActivity::class.java)
                    intent.flags = FLAG_ACTIVITY_SINGLE_TOP
                    intent.flags = FLAG_ACTIVITY_REORDER_TO_FRONT
                    intent.putExtra(getString(R.string.ble_data), doorList)
                    intent.putExtra("BLE_STATE", mState)
                    EventBus.getDefault().postSticky(AuthSuccessEvent(mService!!, mBluetoothAdapter!!))
                    startActivity(intent)
                }
            }
            data == getString(R.string.access_error) -> {
                verifyUser.visibility = View.VISIBLE
            }
        }
    }

    override fun onDataWrite() {
        Log.e("TAG", "Successful data transmit")
        index++
        sendData(dataFrame!!)
    }

    override fun uartNotSupported(action: String) {
        if (action == DEVICE_DOES_NOT_SUPPORT_UART) {
            Toast.makeText(this@BluetoothActivity, getString(R.string.UART_not_supported), Toast.LENGTH_SHORT)
                    .show()
            mService!!.disconnect()
        }
    }

    override val layoutIdForInjection: Int = R.layout.activity_main
    override var broadcastInterface: BroadcastInterface = this

    //BluetoothAdapter instance creation
    private var mDevice: BluetoothDevice? = null
    private var dataFrame: ArrayList<ByteArray>? = null
    private var phoneData: TelephonyManager? = null
    private var passwordHexString = ""
    private var index: Int = 0
    private var baseToolbar: Toolbar? = null
    private var imeiNoHex: String? = null
    private var simSerialHex: String? = null
    private var mobileNoHex: String? = null
    private var doorList: ArrayList<DoorStatusPojo>? = null


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        //toolbar setup
        baseToolbar = findViewById(R.id.base_toolbar)
        setSupportActionBar(baseToolbar)
        mHandler = Handler()
        dataFrame = ArrayList()
        doorList = ArrayList()

        //setting version name
        tvVersionName.append(BuildConfig.VERSION_NAME+" "+DateHelper.getDateTime(System.currentTimeMillis()))
        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        mBluetoothAdapter?.takeIf { !it.isEnabled }?.apply {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }




        deviceConnect.setOnClickListener()
        {
            when {
                !mBluetoothAdapter!!.isEnabled -> {
                    val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
                }
                !hasPermissions(listPermissionsNeeded) -> {
                    ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray(),
                            REQUEST_ID_MULTIPLE_PERMISSIONS)
                }
                else -> {
                    when {
                        deviceConnect.text == getString(R.string.bt_connect) && Preferences.getHassleFreeMode(this)-> {
                            showProgress()
                            mService!!.scanDevices(mBluetoothAdapter!!,mHandler!!)
                        }
                        deviceConnect.text == getString(R.string.bt_connect) ->{
                            showProgress()
                            val intent = Intent(this, DeviceScanActivity::class.java)
                            startActivityForResult(intent, REQUEST_SELECT_DEVICE)
                        }
                        else -> {
                            mService!!.disconnect()
                        }
                    }
                }
            }
        }

        btnSend.setOnClickListener()
        {
            try {
                var flag = true
                when {
                    etMobileNumber.text!!.isEmpty() -> {
                        flag = false
                        etMobileNumber.error = "Enter Mobile No"
                    }
                    etPassword.text!!.isEmpty() -> {
                        flag = false
                        etPassword.error = "Enter Password"
                    }
                    flag -> {
                        showProgress()
                        mHandler!!.postDelayed({
                            hideProgress()
                        }, 4000)
                        dataFrame!!.clear()
                        index = 0

                        imeiNoHex = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            BigInteger(phoneData!!.imei).toString(16)

                        } else {
                            BigInteger(phoneData!!.deviceId).toString(16)

                        }
                        simSerialHex = BigInteger(phoneData!!.simSerialNumber).toString(16)
                        val imeiCommand = getString(R.string.imei_key) + imeiNoHex
                        var simCommand = getString(R.string.sim_no_key) + simSerialHex
                        val mobileCommand = getString(R.string.mobile_key) + mobileNoHex
                        if (simCommand.length > 19) {
                            simCommand = simCommand.substring(0, 19)
                        }

                        Log.e("TAG", "Sim To send" + simCommand)
                        dataFrame!!.add(0, mobileCommand.toByteArray(Charset.defaultCharset()))
                        dataFrame!!.add(1, passwordHexString.toByteArray(Charset.defaultCharset()))
                        dataFrame!!.add(2, imeiCommand.toByteArray(Charset.defaultCharset()))
                        dataFrame!!.add(3, simCommand.toByteArray(Charset.defaultCharset()))

                        sendData(dataFrame!!)
                    }
                }

            } catch (e: Exception) {
                hideProgress()
                e.printStackTrace()
                Log.e("TAG", "Text exception ")
            }
        }
        etMobileNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (etMobileNumber.text.toString().isNotEmpty()) {
                    mobileNoHex = etMobileNumber.text.toString().toLong().toString(16)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        etPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (etPassword.text.toString().isNotEmpty()) {
                    passwordHexString = getString(R.string.pass_key) + Integer.toHexString(Integer.parseInt(etPassword.text.toString()))
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {

            when (requestCode) {
                REQUEST_SELECT_DEVICE -> {
                    val deviceAddress = data.getStringExtra(BluetoothDevice.EXTRA_DEVICE)
                    mDevice = mBluetoothAdapter!!.getRemoteDevice(deviceAddress)
                    connectionData.visibility = View.VISIBLE
                    connectionStatus.text = "Connecting to " + mDevice!!.name
                    mService!!.connect(deviceAddress, mBluetoothAdapter,this)
                }
            }
        }
    }

    private fun sendData(dataFrame: ArrayList<ByteArray>) {
        Timer().schedule(object : TimerTask() {
            override fun run() {
                if (dataFrame.isNotEmpty() && index < dataFrame.size) {
                    Log.e("TAG", "Inside send data")
                    mService!!.writeRXCharacteristic(dataFrame[index])
                } else {
                    hideProgress()
                }
            }
        }, 300)

    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onResume() {
        super.onResume()
        //to check if bluetooth connected
        if(mState==BLE_PROFILE_CONNECTED)
        {
            mService!!.disconnect()
        }
        ArchLifecycleApp.Status.activityResumed()
        hideProgress()
        doorList = ArrayList()
        etMobileNumber.text!!.clear()
        etPassword.text!!.clear()

    }

    override fun onPause() {
        super.onPause()
        ArchLifecycleApp.Status.activityPaused()
    }

    //loader setup
    private fun showProgress() {
        runOnUiThread {
            transmitDataProgressbar.visibility = View.VISIBLE
        }
    }

    fun hideProgress() {
        runOnUiThread {
            transmitDataProgressbar.visibility = View.GONE
        }

    }

}
