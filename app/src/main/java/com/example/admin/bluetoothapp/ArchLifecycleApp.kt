package com.example.admin.bluetoothapp


import android.app.Application
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ProcessLifecycleOwner
import android.util.Log
import com.example.admin.bluetoothapp.utility.AppBackgroundEvents
import com.example.admin.bluetoothapp.utility.Preferences
import org.greenrobot.eventbus.EventBus

class  ArchLifecycleApp:Application(),LifecycleObserver {

    override fun onCreate() {
        super.onCreate()
        //to manage lifecycle of whole application process
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

   //application lifecycle methods
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun connectListener() {
       Preferences.setAppSession(this,false)
        Log.e("TAG","App in foreground")

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun disconnectListener() {
        Preferences.setAppSession(this,true)
        val isHassleFree= Preferences.getHassleFreeMode(this)
        Log.e("TAG","App in background")
        if(isHassleFree)
        {
            EventBus.getDefault().post(AppBackgroundEvents())
        }

    }

object Status{
    fun isActivityVisible(): Boolean {
        return activityVisible
    }

    fun activityResumed() {
        activityVisible = true
    }

    fun activityPaused() {
        activityVisible = false
    }

    private var activityVisible: Boolean = false
}


}