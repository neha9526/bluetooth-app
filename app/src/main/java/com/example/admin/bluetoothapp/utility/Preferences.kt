package com.example.admin.bluetoothapp.utility

import android.content.Context
import android.content.SharedPreferences

class Preferences {

    companion object {
        private val AUTHENTICATION = "authentication"
        private val AUTH_DATA = "auth_data"
        private val USER_SESSION ="user_session"
        private val MOBILE_NUMBER ="mobile_number"
        private val IMEI_SESSION = "imei_session"
        private val IMEI_NUMBER ="imie_number"
        private val SERIAL_NUMBER ="serial_number"
        private val SERIAL_SESSION ="serial_session"
        private val LOCK_NAME_SESSION ="lock_name_session"
        private val LOCK_NAME = "lock_name"
        private val HASSLE_FREE_MODE_SESSION = "entry_mode_session"
        private val HASSLE_FREE_MODE ="entry_mode"
        private val APP_SESSION ="app_session"
        private val IS_APP_IN_BG ="is_app_in_bg"

        //shared preference for authentication response
        fun setAuthData(context: Context, authData:String) {
            val sharedPreferences = context.getSharedPreferences(AUTHENTICATION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(AUTH_DATA, authData)
            editor.apply()
        }

        fun getAuthData(context: Context): String {
            val sharedPreferences = context.getSharedPreferences(AUTHENTICATION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(AUTH_DATA, "")
        }

        //shared preference for authentication response
        fun setMobileNo(context: Context, mobileNo:String) {
            val sharedPreferences = context.getSharedPreferences(USER_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(MOBILE_NUMBER, mobileNo)
            editor.apply()
        }
        fun getMobileNo(context: Context): String {
            val sharedPreferences = context.getSharedPreferences(USER_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(MOBILE_NUMBER, "")
        }

        //set imei no
        fun setImeiNo(context: Context,imei:String)
        {
            val sharedPreferences = context.getSharedPreferences(IMEI_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(IMEI_NUMBER, imei)
            editor.apply()
        }
        fun getImeiNo(context: Context): String {
            val sharedPreferences = context.getSharedPreferences(IMEI_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(IMEI_NUMBER, "")
        }
        //set serial no
        fun setSerialNo(context: Context,serialNo:String)
        {
            val sharedPreferences = context.getSharedPreferences(SERIAL_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(SERIAL_NUMBER, serialNo)
            editor.apply()
        }
        fun getSerialNo(context: Context): String {
            val sharedPreferences = context.getSharedPreferences(SERIAL_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(SERIAL_NUMBER, "")
        }

        //set Lock name
        fun setLockName(context: Context,lockName:String)
        {
            val sharedPreferences = context.getSharedPreferences(LOCK_NAME_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(LOCK_NAME, lockName)
            editor.apply()
        }
        fun getLockName(context: Context): String {
            val sharedPreferences = context.getSharedPreferences(LOCK_NAME_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getString(LOCK_NAME, "")
        }
      //to set and get entry mode for application
        fun setHassleFreeMode(context: Context,mode:Boolean)
      {
          val sharedPreferences:SharedPreferences = context.getSharedPreferences(HASSLE_FREE_MODE_SESSION, Context.MODE_PRIVATE)
          val editor = sharedPreferences.edit()
          editor.putBoolean(HASSLE_FREE_MODE,mode)
          editor.apply()
      }

        fun getHassleFreeMode(context: Context):Boolean
        {
            val sharedPreferences = context.getSharedPreferences(HASSLE_FREE_MODE_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getBoolean(HASSLE_FREE_MODE, false)
        }

        //app session
        fun setAppSession(context: Context,isAppInBg:Boolean)
        {
            val sharedPreferences:SharedPreferences = context.getSharedPreferences(APP_SESSION, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putBoolean(IS_APP_IN_BG,isAppInBg)
            editor.apply()
        }

        fun getAppSession(context: Context):Boolean
        {
            val sharedPreferences = context.getSharedPreferences(APP_SESSION, Context.MODE_PRIVATE)
            return sharedPreferences.getBoolean(IS_APP_IN_BG, false)
        }
    }

}