package com.example.admin.bluetoothapp

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.*
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_RESTART_SCAN
import com.example.admin.bluetoothapp.GATTConstants.Constants.DEVICE_LIST
import com.example.admin.bluetoothapp.utility.AppBackgroundEvents
import com.example.admin.bluetoothapp.utility.Preferences
import kotlinx.android.synthetic.main.activity_base_appcompact_acivity.*
import kotlinx.android.synthetic.main.base_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.nio.charset.Charset
import java.util.*
import kotlin.collections.ArrayList

abstract class BaseAppcompactAcivity : AppCompatActivity() {

    abstract val layoutIdForInjection: Int
    abstract val broadcastInterface: BroadcastInterface

    var mBluetoothAdapter: BluetoothAdapter? = null
    var mService: BleService? = null
    var mHandler: Handler? = null
    var isAppInBg: Boolean = true
    var mState = BLE_PROFILE_DISCONNECTED
    var lockStatus = -1
    private var index: Int = 0
    private var dataFrame: ArrayList<ByteArray>? = null
    val listPermissionsNeeded = ArrayList<String>()
    var hassleFreeMode: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_appcompact_acivity)
        setSupportActionBar(base_toolbar)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        mHandler = Handler()

        //layout container`set up
        val lytId = layoutIdForInjection
        if (lytId != -1) {
            LayoutInflater.from(this).inflate(lytId, container)
        }
        //Actionbar set up
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        with(supportActionBar!!) {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            base_toolbar.setNavigationOnClickListener {
                onBackpressEvent()
            }
        }
        //fine location and Course location, BT permission
        listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE)


        // Use this check to determine whether BLE is supported on the device. Then
        // you can selectively disable BLE-related features.
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show()
            finish()
        } else {
            val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            mBluetoothAdapter = bluetoothManager.adapter
        }

        //init service
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, makeGattUpdateIntentFilter())
        val bindIntent = Intent(this, BleService::class.java)
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE)
    }

    //UART service connected/disconnected
    val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, rawBinder: IBinder) {
            mService = (rawBinder as BleService.LocalBinder).getBleService()
            Log.e("TAG", "onServiceConnected mService= $mService")
        }

        override fun onServiceDisconnected(classname: ComponentName) {
            mService!!.disconnect()
            mService = null
        }
    }

    //Actions to be handled by BroadCast receiver
    private fun makeGattUpdateIntentFilter(): IntentFilter {
        val intentFilter = IntentFilter()
        intentFilter.addAction(GATTConstants.Constants.ACTION_DEVICE_FOUND)
        intentFilter.addAction(GATTConstants.Constants.ACTION_GATT_CONNECTED)
        intentFilter.addAction(GATTConstants.Constants.ACTION_GATT_DISCONNECTED)
        intentFilter.addAction(GATTConstants.Constants.ACTION_GATT_SERVICES_DISCOVERED)
        intentFilter.addAction(GATTConstants.Constants.ACTION_DATA_AVAILABLE)
        intentFilter.addAction(GATTConstants.Constants.DEVICE_DOES_NOT_SUPPORT_UART)
        intentFilter.addAction(GATTConstants.Constants.ACTION_DATA_WRITE)
        //action added for restarting service
        intentFilter.addAction(ACTION_RESTART_SCAN)
        return intentFilter
    }

    //broadcast manager set up
    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        @SuppressLint("MissingPermission")
        override fun onReceive(p0: Context?, p1: Intent?) {
            hassleFreeMode = Preferences.getHassleFreeMode(this@BaseAppcompactAcivity)
            isAppInBg = Preferences.getAppSession(this@BaseAppcompactAcivity)
            val action = p1!!.action
            //*********************//
            when (action) {
                 GATTConstants.Constants.ACTION_GATT_CONNECTED -> {
                    mState = BLE_PROFILE_CONNECTED
                     val device =  p1.extras!!.get(GATTConstants.Constants.EXTRA_DATA) as BluetoothDevice
                    broadcastInterface.onBleConnected(device)
                }

                GATTConstants.Constants.ACTION_GATT_DISCONNECTED -> {
                    Log.e("TAG", "Device Disconnected")
                    val device =  p1.extras!!.get(GATTConstants.Constants.EXTRA_DATA) as BluetoothDevice
                    mState = BLE_PROFILE_DISCONNECTED
                    if (isAppInBg && hassleFreeMode) {
                        mHandler!!.postDelayed({
                            mService!!.scanDevices(mBluetoothAdapter!!, mHandler!!)
                        }, 20000)
                    }
                    else{
                        broadcastInterface.onBleDisconnected(device)
                    }
                }

                GATTConstants.Constants.ACTION_GATT_SERVICES_DISCOVERED -> {
                    Log.e("TAG", "getSupportedGattServices " + mService!!.getSupportedGattServices())
                    mService!!.enableTXNotification()
                    if (isAppInBg && hassleFreeMode) {
                        index = 0
                        Log.e("TAG", "Device connected")
                        dataFrame = ArrayList()
                        val mobComm = getString(R.string.hassel_free_comm) + Preferences.getMobileNo(this@BaseAppcompactAcivity)
                        val imeiComm = getString(R.string.hassel_free_comm) + Preferences.getImeiNo(this@BaseAppcompactAcivity)
                        var serialNo = getString(R.string.hassel_free_comm) + Preferences.getSerialNo(this@BaseAppcompactAcivity)
                        if(serialNo.length>19)
                        {
                            serialNo = serialNo.substring(0,19)
                        }
                        dataFrame!!.add(mobComm.toByteArray(Charset.defaultCharset()))
                        dataFrame!!.add(imeiComm.toByteArray(Charset.defaultCharset()))
                        dataFrame!!.add(serialNo.toByteArray(Charset.defaultCharset()))
                        Log.e("TAG", "Dataframe List " + dataFrame)
                        sendHasselfreeData(dataFrame!!)

                    } else {
                        broadcastInterface.onServiceDiscovered(action)
                    }
                }
                GATTConstants.Constants.ACTION_DATA_AVAILABLE -> {
                    val txValue = p1.getByteArrayExtra(GATTConstants.Constants.EXTRA_DATA)
                    val response: String = txValue.toString(Charset.defaultCharset())
                    if (isAppInBg && hassleFreeMode) {
                        Log.e("TAG", "Received data " + response)
                        when {
                                response.contains(getString(R.string.door_close_master)) ||
                                        response.contains(getString(R.string.door_close_normal))-> {
                                val timeStamp: Long = System.currentTimeMillis()
                                val hexTimeStamp = timeStamp.toString(16)
                                val hexCommand = getString(R.string.door_open_key)+"1"+hexTimeStamp
                                mService!!.writeRXCharacteristic(hexCommand.toByteArray(Charset.defaultCharset()))
                            }
                            response.contains(getString(R.string.door_open_normal)) ||
                                    response.contains(getString(R.string.door_open_master)) -> {
                            }
                        }
                    } else {
                        broadcastInterface.onDataAvailable(response)
                    }
                }

                //on successful data write
                GATTConstants.Constants.ACTION_DATA_WRITE -> {
                    if (isAppInBg && hassleFreeMode) {
                        Log.e("TAG", "Successful data write")
                        index++
                        sendHasselfreeData(dataFrame!!)
                    } else {
                        broadcastInterface.onDataWrite()
                    }
                }
                //*****When service is not available**********//
                GATTConstants.Constants.DEVICE_DOES_NOT_SUPPORT_UART -> {
                    broadcastInterface.uartNotSupported(action)
                }
                //action when device is found
                GATTConstants.Constants.ACTION_DEVICE_FOUND -> {
                    val deviceList: ArrayList<BluetoothDevice> = p1.getParcelableArrayListExtra(DEVICE_LIST)
                    for (device in deviceList) {
                        mService!!.connect(device.address, mBluetoothAdapter,this@BaseAppcompactAcivity)
                        break
                       // val deviceName = mBluetoothAdapter!!.getRemoteDevice(device.address).name
                       /* if (deviceName == Preferences.getLockName(this@BaseAppcompactAcivity)) {
                            Log.e("TAG", "mState-->$mState")

                        }*/
                    }
                }
                ACTION_RESTART_SCAN -> {
                    mService!!.scanDevices(mBluetoothAdapter!!, mHandler!!)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }

        try {
            mService!!.unbindService(mServiceConnection)
            // LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        } catch (ignore: Exception) {
            Log.e("TAG", ignore.toString())
        }
    }


    fun onBackpressEvent() {
        mService!!.disconnect()
        finish()
    }


    fun sendHasselfreeData(dataFrame: ArrayList<ByteArray>) {
        if (dataFrame.isNotEmpty() && index < dataFrame.size) {
            Timer().schedule(object : TimerTask() {
                override fun run() {
                    try {
                        mService!!.writeRXCharacteristic(dataFrame[index])
                    }
                    catch (e:java.lang.Exception)
                    {
                        Log.e("TAG","writeRXCharacteristic exception "+e.cause)
                    }

                }

            }, 300)
        }

    }

    override fun onResume() {
        super.onResume()

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        when {
            !mBluetoothAdapter!!.isEnabled -> {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
            !hasPermissions(listPermissionsNeeded) -> {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray
                (), REQUEST_ID_MULTIPLE_PERMISSIONS)
            }
        }
    }

    //method to check location permission
    fun hasPermissions(permissions: ArrayList<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && applicationContext != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(applicationContext, permission) !=
                        PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }


    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun scanDeviceEvent(appBackgroundEvents: AppBackgroundEvents) {
        if(mBluetoothAdapter!=null)
        {
            mService!!.scanDevices(mBluetoothAdapter!!,mHandler!!)
            Log.e("TAG", "Inside ScanDevice Event")
        }

        /*if(mService!!.mBluetoothGatt!!.connect())
        {
            index = 0
            Log.e("TAG", "Device connected")
            dataFrame = ArrayList()
            val mobComm = getString(R.string.hassel_free_comm) + Preferences.getMobileNo(this@BaseAppcompactAcivity)
            val imeiComm = getString(R.string.hassel_free_comm) + Preferences.getImeiNo(this@BaseAppcompactAcivity)
            var serialNo = getString(R.string.hassel_free_comm) + Preferences.getSerialNo(this@BaseAppcompactAcivity)
            if(serialNo.length>19)
            {
                serialNo = serialNo.substring(0,19)
            }
            dataFrame!!.add(mobComm.toByteArray(Charset.defaultCharset()))
            dataFrame!!.add(imeiComm.toByteArray(Charset.defaultCharset()))
            dataFrame!!.add(serialNo.toByteArray(Charset.defaultCharset()))
            Log.e("TAG", "Dataframe List " + dataFrame)
            sendHasselfreeData(dataFrame!!)
        }
        else{
        }*/


    }


    override fun onBackPressed() {
        super.onBackPressed()
        onBackpressEvent()
    }
}
