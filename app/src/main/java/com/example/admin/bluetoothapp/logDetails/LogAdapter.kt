package com.example.admin.bluetoothapp.logDetails

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.admin.bluetoothapp.R
import com.example.admin.bluetoothapp.utility.DateHelper
import com.example.admin.bluetoothapp.utility.LogDetailsPojo
import kotlinx.android.synthetic.main.adapter_log_details.view.*

class LogAdapter(private val logList:ArrayList<LogDetailsPojo>,
                 private val context: Context) : RecyclerView.Adapter<LogAdapter.LogView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogView {
            val viewHolder = LayoutInflater.from(parent.context).inflate(R.layout.adapter_log_details, parent,
                    false)
            return LogView(viewHolder)
    }

    override fun getItemCount(): Int {
      return  logList.size
    }

    override fun onBindViewHolder(holder: LogView, position: Int) {
        holder.mobileNo.text = logList[position].mobileList.toString()
        holder.time.text = DateHelper.getDateTime( logList[position].timeList)
        Log.e("TAG","Time stamp long "+ logList[position].timeList)
        Log.e("TAG","Time actual "+DateHelper.getDateTime( logList[position].timeList))
        holder.latchName.text = logList[position].latchName
        if(logList[position].action == 0)
        {
            holder.actionStatus.setImageResource(R.drawable.ic_lock2)
        }
        else{
            holder.actionStatus.setImageResource(R.drawable.ic_opened_lock3)
        }
    }


    class LogView(item:View): RecyclerView.ViewHolder(item) {
         val mobileNo = item.mobNoData!!
         val time = item.timeData!!
         val actionStatus = item.actionStatus!!
         val latchName:AppCompatTextView = item.tvLatchName!!
    }
}