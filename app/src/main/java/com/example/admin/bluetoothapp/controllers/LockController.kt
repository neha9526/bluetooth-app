package com.example.admin.bluetoothapp.controllers

import android.content.Context
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.example.admin.bluetoothapp.R
import com.example.admin.bluetoothapp.pojo.DoorStatusPojo
import com.example.admin.bluetoothapp.utility.AllResponseEvent
import com.example.admin.bluetoothapp.utility.WriteDataEvent
import kotlinx.android.synthetic.main.layout_door_controller.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class LockController : Controller(), AdapterView.OnItemSelectedListener {

    var context: Context? = null
    var dataFrameList: ArrayList<ByteArray>? = null
    var doorList: ArrayList<DoorStatusPojo>? = null
    var doorLockNameList: ArrayList<String>? = null
    var selectedDoorNo: String = "1"
    var mHandler: Handler? = null
    var doorName: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        context = applicationContext!!
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        return inflater.inflate(R.layout.layout_door_controller, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        //setting up spinner*/
        doorList = ArrayList()
        doorList = parentController!!.args.getParcelableArrayList("DoorList")
        mHandler = Handler()

        if (doorList!!.isNotEmpty()) {
            doorLockNameList = ArrayList()
            for (door in doorList!!) {
                doorLockNameList!!.add(door.lockName)
            }
        }
        if (doorLockNameList!!.isNotEmpty()) {
            when {
                doorLockNameList!!.size > 1 -> {
                    view.doorListSpinner.visibility = View.VISIBLE
                    val doorListAdapter = ArrayAdapter(context, R.layout.textviewforspinner, doorLockNameList)
                    view.doorListSpinner.adapter = doorListAdapter
                    view.doorListSpinner.onItemSelectedListener = this
                }
                else -> {

                    when {
                        doorList!![0].lockStatus.contains(context!!.getString(R.string.door_open_master)) ||
                                doorList!![0].lockStatus.contains(context!!.getString(R.string.door_open_normal)) -> {
                            doorName = doorList!![0].lockName
                            lockSetUp(doorName + " " + context!!.getString(R.string.door_unlock), R.drawable.ic_opened_lock2)
                        }

                        doorList!![0].lockStatus.contains(context!!.getString(R.string.door_close_master)) ||
                                doorList!![0].lockStatus.contains(context!!.getString(R.string.door_close_master)) -> {
                            doorName = doorList!![0].lockName
                            lockSetUp(doorName + " " + context!!.getString(R.string.door_locked), R.drawable.ic_lock2)
                        }
                    }
                }
            }
        }


        view.lock_status_img.setOnClickListener()
        {
            showProgress(view)
            mHandler!!.postDelayed({
                hideProgress()
            }, 4000)
            dataFrameList = ArrayList()
            //getting timestamp in seconds
            val timeStamp: Long = System.currentTimeMillis()/1000
            val hexTimeStamp = timeStamp.toString(16)
            Log.e("TAG","Timestamp converted "+hexTimeStamp)
            when {
                view.lock_status_text.text.toString().contains(context!!.getString(R.string.door_locked)) -> {
                    val hexCommand = context!!.getString(R.string.door_open_key) + selectedDoorNo + hexTimeStamp
                    Log.e("TAG","Timestamp command "+hexCommand)
                    dataFrameList!!.add(hexCommand.toByteArray())
                    EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                }
                view.lock_status_text.text.toString().contains(context!!.getString(R.string.door_unlock)) -> {
                    val hexCommand = context!!.getString(R.string.door_close_key) + selectedDoorNo + hexTimeStamp
                    Log.e("TAG","Timestamp command "+hexCommand)
                    dataFrameList!!.add(hexCommand.toByteArray())
                    EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                }
            }
        }
    }

    //Subscriber to listen response
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun changeLockStatus(allResponseEvent: AllResponseEvent) {
        hideProgress()
        Log.e("TAG", "Inside changeLockStatus response " + allResponseEvent.mService)
        //need response as CM or OM to update lock status only
        if(view!!.doorListSpinner.visibility == View.VISIBLE)
        {
            doorList!![view!!.doorListSpinner.selectedItemPosition].lockStatus = allResponseEvent.response
        }

        when {
            allResponseEvent.response.contains(context!!.getString(R.string.door_open_master)) ||
                    allResponseEvent.response.contains(context!!.getString(R.string.door_open_normal)) -> {
                lockSetUp(doorName + " " + context!!.getString(R.string.door_unlock), R.drawable.ic_opened_lock2)
            }
            allResponseEvent.response.contains(context!!.getString(R.string.door_close_master)) ||
                    allResponseEvent.response.contains(context!!.getString(R.string.door_close_normal)) -> {
                lockSetUp(doorName + " " + context!!.getString(R.string.door_locked), R.drawable.ic_lock2)
            }
            allResponseEvent.response.equals("ACCESS ERROR") -> {
                Toast.makeText(applicationContext, "Your access is expired", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun lockSetUp(status: String, imgInt: Int) {
        view!!.lock_status_img.setImageResource(imgInt)
        view!!.lock_status_text.text = status
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        selectedDoorNo = doorList!![p2].lockNumber
        when (doorList!![p2].lockStatus) {
            context!!.getString(R.string.door_close_master),
            context!!.getString(R.string.door_close_normal) -> {
                doorName = doorList!![p2].lockName
                lockSetUp(doorName + " " + context!!.getString(R.string.door_locked), R.drawable.ic_lock2)
            }
            context!!.getString(R.string.door_open_normal),
            context!!.getString(R.string.door_open_master) -> {
                doorName = doorList!![p2].lockName
                lockSetUp(doorName + " " + context!!.getString(R.string.door_locked), R.drawable.ic_opened_lock2)
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {}

    private fun showProgress(view: View) {
        view.doorOpenCloseLoader.visibility = View.VISIBLE
        activity!!.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

    }

    private fun hideProgress() {
        try{
            if(view!!.doorOpenCloseLoader.visibility==View.VISIBLE)
            {
                view!!.doorOpenCloseLoader.visibility = View.GONE
                activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            }
        }
        catch (e:Exception)
        {
            Log.e("TAG","Exception loading "+e.message)
        }


    }
}