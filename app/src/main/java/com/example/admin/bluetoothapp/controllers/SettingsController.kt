package com.example.admin.bluetoothapp.controllers

import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatTextView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bluelinelabs.conductor.Controller
import com.example.admin.bluetoothapp.BroadcastInterface
import com.example.admin.bluetoothapp.R
import com.example.admin.bluetoothapp.pojo.DoorStatusPojo
import com.example.admin.bluetoothapp.utility.AllResponseEvent
import com.example.admin.bluetoothapp.utility.ChangeDetailsEvent
import com.example.admin.bluetoothapp.utility.Preferences
import com.example.admin.bluetoothapp.utility.WriteDataEvent
import kotlinx.android.synthetic.main.layout_door_controller.view.*
import kotlinx.android.synthetic.main.layout_settings_controller.view.*
import kotlinx.android.synthetic.main.layout_user_controller.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.nio.charset.Charset
import java.util.*


class SettingsController:Controller(){

    private var context: Context? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this)
        }
        return inflater.inflate(R.layout.layout_settings_controller, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        context = applicationContext!!



        //change mobile no
        view.changeMobNo.setOnClickListener {
            val inflater = LayoutInflater.from(activity!!)
            val dialogView = inflater.inflate(R.layout.user_popup, null)
            val alertTitle = dialogView.findViewById(R.id.commandName) as AppCompatTextView
            val userData = dialogView.findViewById(R.id.commandData) as AppCompatEditText
            val btnSend = dialogView.findViewById(R.id.send_user) as AppCompatButton
            val oldPassword = dialogView.findViewById<AppCompatEditText>(R.id.oldPassword)
            oldPassword.visibility = View.VISIBLE
            alertTitle.text = "Change Mobile No"
            val popUpBuilder = AlertDialog.Builder(activity!!)
            popUpBuilder.setView(dialogView)
            popUpBuilder.setCancelable(true)
            val alertDialog = popUpBuilder.create()
            alertDialog.show()

            btnSend.setOnClickListener {
                if ( oldPassword.text.toString().isNotEmpty()&& userData.text.toString().isNotEmpty()) {
                    val dataFrameList:ArrayList<ByteArray> = ArrayList()
                    val mobHex =  "080"+ userData.text.toString().toLong().toString(16)
                    val passwordHex = "081"+oldPassword.text.toString().toLong().toString(16)
                    val imeiNo = "082"+Preferences.getImeiNo(context!!)
                    val simSerialNo = "083"+Preferences.getSerialNo(context!!)


                    dataFrameList.add(mobHex.toByteArray(Charset.defaultCharset()))
                    dataFrameList.add(passwordHex.toByteArray(Charset.defaultCharset()))
                    dataFrameList.add(imeiNo.toByteArray(Charset.defaultCharset()))
                    dataFrameList.add(simSerialNo.toByteArray(Charset.defaultCharset()))

                    EventBus.getDefault().post(WriteDataEvent(dataFrameList))
                    alertDialog.dismiss()
                } else {
                    Toast.makeText(context, context!!.getString(R.string.enter_mob_no), Toast.LENGTH_SHORT).show()
                }
            }
        }


        //change password
        view.changePassword.setOnClickListener {
            val inflater = LayoutInflater.from(activity!!)
            val dialogView = inflater.inflate(R.layout.user_popup, null)
            val alertTitle = dialogView.findViewById(R.id.commandName) as AppCompatTextView
            val userData = dialogView.findViewById(R.id.commandData) as AppCompatEditText
            val btnSend = dialogView.findViewById(R.id.send_user) as AppCompatButton
            val oldPassword = dialogView.findViewById<AppCompatEditText>(R.id.oldPassword)
            oldPassword.visibility = View.VISIBLE
            userData.visibility = View.GONE
            alertTitle.text = "Change Password"
            val popUpBuilder = AlertDialog.Builder(activity!!)
            popUpBuilder.setView(dialogView)
            popUpBuilder.setCancelable(true)
            val alertDialog = popUpBuilder.create()
            alertDialog.show()

            btnSend.setOnClickListener {
                if ( oldPassword.text.toString().isNotEmpty()) {
                    val dataFrameList:ArrayList<ByteArray> = ArrayList()
                    val mobHex = "080" + Preferences.getMobileNo(context!!)
                    val passwordHex = "081" + oldPassword.text.toString().toLong().toString(16)
                    val imeiNo = "082"+Preferences.getImeiNo(context!!)
                    val simSerialNo = "083"+Preferences.getSerialNo(context!!)


                    dataFrameList.add(mobHex.toByteArray(Charset.defaultCharset()))
                    dataFrameList.add(passwordHex.toByteArray(Charset.defaultCharset()))
                    dataFrameList.add(imeiNo.toByteArray(Charset.defaultCharset()))
                    dataFrameList.add(simSerialNo.toByteArray(Charset.defaultCharset()))

                    EventBus.getDefault().post(WriteDataEvent(dataFrameList))

                    alertDialog.dismiss()
                } else {
                    Toast.makeText(context, context!!.getString(R.string.enter_mob_no), Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this)
        }
    }

    //even listener
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun listenResponse(allResponseEvent: AllResponseEvent)
    {
        when(allResponseEvent.response)
        {
            "ERROR" ->{
                Toast.makeText(context, "Invalid User", Toast.LENGTH_SHORT).show()
            }
            "OK MU" ->{
                Toast.makeText(context, "User details updated", Toast.LENGTH_SHORT).show()
            }
        }
    }


}
