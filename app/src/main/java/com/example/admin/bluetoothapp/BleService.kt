package com.example.admin.bluetoothapp

import android.app.Service
import android.bluetooth.*
import android.bluetooth.BluetoothAdapter.*
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.*
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_DATA_AVAILABLE
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_DATA_WRITE
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_DEVICE_FOUND
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_GATT_CONNECTED
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_GATT_DISCONNECTED
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_GATT_SERVICES_DISCOVERED
import com.example.admin.bluetoothapp.GATTConstants.Constants.ACTION_RESTART_SCAN
import com.example.admin.bluetoothapp.GATTConstants.Constants.CCCD
import com.example.admin.bluetoothapp.GATTConstants.Constants.DEVICE_DOES_NOT_SUPPORT_UART
import com.example.admin.bluetoothapp.GATTConstants.Constants.DEVICE_LIST
import com.example.admin.bluetoothapp.GATTConstants.Constants.EXTRA_DATA
import com.example.admin.bluetoothapp.GATTConstants.Constants.RX_CHAR_UUID
import com.example.admin.bluetoothapp.GATTConstants.Constants.RX_SERVICE_UUID
import com.example.admin.bluetoothapp.GATTConstants.Constants.TX_CHAR_UUID
import com.example.admin.bluetoothapp.utility.Preferences

class BleService : Service() {

    private val mBinder = LocalBinder()
    private var mBluetoothManager: BluetoothManager? = null
    private var mBluetoothDeviceAddress: String? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null
    var mBluetoothGatt: BluetoothGatt? = null
    private var mConnectionState = STATE_DISCONNECTED
    private var bgScanList: ArrayList<BluetoothDevice>? = null
    private var bgDeviceList: ArrayList<String>? = null
    var bluetoothLeScanner: BluetoothLeScanner? = null
    var mHandler: Handler? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }


    private val mGattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            val intentAction: String
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    intentAction = ACTION_GATT_CONNECTED
                    mConnectionState = STATE_CONNECTED
                    broadcastUpdate(intentAction, gatt!!.device)
                    // Attempts to discover services after successful connection.
                    mBluetoothGatt!!.discoverServices()
                    Log.e("TAG", "Connected to GATT server.")
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    intentAction = ACTION_GATT_DISCONNECTED
                    mConnectionState = STATE_DISCONNECTED
                    broadcastUpdate(intentAction, gatt!!.device)
                    Log.i(TAG, "Disconnected from GATT server.")

                }
            }

        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            when (status) {
                BluetoothGatt.GATT_SUCCESS -> {
                    broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED)
                    Log.e("TAG", "onServicesDiscovered received: $status")
                }
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt,
                                          characteristic: BluetoothGattCharacteristic,
                                          status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.e("TAG", "onCharacteristicRead $characteristic")
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic)
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
            Log.e("TAG", "Inside onCharacteristic Changed")
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic)

        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_WRITE)
            }
        }
    }


    private fun broadcastUpdate(action: String) {
        val intent = Intent(action)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun broadcastUpdate(action: String, characteristic: BluetoothGattCharacteristic) {
        val intent = Intent(action)
        // This is handling for the notification on TX Character of NUS service
        if (characteristic.uuid == TX_CHAR_UUID) {
            intent.putExtra(EXTRA_DATA, characteristic.value)
        } else {
            Log.e("TAG", "No data found")
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }


    private fun broadcastUpdate(action: String, mBluetoothDevice: BluetoothDevice?) {
        val intent = Intent(action)
        // This is handling for the notification on TX Character of NUS service
        if (mBluetoothDevice != null) {
            intent.putExtra(EXTRA_DATA, mBluetoothDevice)
        } else {
            Log.e("TAG", "No data found")
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }


    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */

    fun connect(address: String?, bluetoothAdapter: BluetoothAdapter?, context: Context): Boolean {
        try{
            mBluetoothAdapter = bluetoothAdapter
            val device = bluetoothAdapter!!.getRemoteDevice(address)
            if (mBluetoothAdapter == null || address == null) {
                broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART)
                return false
            }
            // Previously connected device.  Try to reconnect.
            if (mBluetoothDeviceAddress != null && address == mBluetoothDeviceAddress && mBluetoothGatt != null) {
                Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.")
                if (mBluetoothGatt!!.connect()) {
                    mBluetoothGatt = device.connectGatt(this, false, mGattCallback)
                    mConnectionState = STATE_CONNECTING
                    return true
                }
            }

            if (device == null) {
                Log.w(TAG, "Device not found.  Unable to connect.")
                return false
            }
            // Wea want to directly connect to the device, so we are setting the autoConnect // parameter to false.
            mBluetoothGatt = when (Preferences.getHassleFreeMode(context)) {
                true -> {
                    device.connectGatt(this, true, mGattCallback)
                }
                false -> {
                    device.connectGatt(this, false, mGattCallback)
                }
            }
            Log.e("TAG", "Trying to create a new connection.")
            mBluetoothDeviceAddress = address
            mConnectionState = STATE_CONNECTING
            return true
        }
        catch (e:Exception)
        {
            Toast.makeText(this,"Connecting Exception "+e.message,Toast.LENGTH_SHORT).show();
            return  false;
        }
    }


    //  function to get available GATT services
    fun getSupportedGattServices(): List<BluetoothGattService>? {
        return if (mBluetoothGatt == null) null else mBluetoothGatt!!.services
    }

    /**After using a given BLE device, the app must call this method to ensure resources are
     * released properly.*/
    fun releaseResources() {
        if (mBluetoothGatt == null) {
            return
        }
        Log.w(TAG, "mBluetoothGatt closed")
        mBluetoothDeviceAddress = null
        mBluetoothGatt!!.close()
        mBluetoothGatt = null

    }

    /**Request a read on a given `BluetoothGattCharacteristic`. The read result is reported
     * asynchronously through the `BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt,
     * android.bluetooth.BluetoothGattCharacteristic, int)`callback.@param characteristic The characteristic to read from.
     */
    fun readCharacteristic(characteristic: BluetoothGattCharacteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.e("TAG", "readCharacteristic BluetoothAdapter not initialized")
            return
        }
        mBluetoothGatt!!.readCharacteristic(characteristic)
    }

    /*Enables or disables notification on a give characteristic.
     * Enable Notification on TX characteristic * @return  */

    fun enableTXNotification() {
        val rxService = mBluetoothGatt!!.getService(RX_SERVICE_UUID)
        if (rxService == null) {
            Log.e("TAG", "Inside enableTXNotification rxService null")
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART)
            return
        }
        val txChar = rxService.getCharacteristic(TX_CHAR_UUID)
        if (txChar == null) {
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART)
            return
        }
        mBluetoothGatt!!.setCharacteristicNotification(txChar, true)
        mBluetoothGatt!!.readCharacteristic(txChar)
        val descriptor = txChar.getDescriptor(CCCD)
        descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        mBluetoothGatt!!.writeDescriptor(descriptor)
        Log.e("TAG", "EnabledTXNotification rxService")
    }

    //to write data to bluetooth module
    fun writeRXCharacteristic(value: ByteArray) {
        Log.e("TAG", "mBluetoothGatt " + mBluetoothGatt)

        val rxService = mBluetoothGatt!!.getService(RX_SERVICE_UUID)

        if (rxService == null) {
            Log.e("TAG", "Inside rxService null")
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART)
            return
        }
        val rxChar = rxService.getCharacteristic(RX_CHAR_UUID)
        if (rxChar == null) {
            Log.e("TAG", "Inside rxChar null")
            broadcastUpdate(DEVICE_DOES_NOT_SUPPORT_UART)
            return
        }

        rxChar.value = value
        val status = mBluetoothGatt!!.writeCharacteristic(rxChar)
        Log.e("TAG", "write TXchar - status=" + status)

    }

    // disconnects current and pending connection
    fun disconnect() {
        try{
            if ( mBluetoothGatt == null) {
                Log.w(TAG, "BluetoothAdapter not initialized")
                return
            }
            mBluetoothGatt!!.disconnect()
        }
        catch(e:Exception)
        {
            Toast.makeText(this,"Exception "+e.message,Toast.LENGTH_SHORT).show()
        }


    }

    //overriden methods
    override fun onBind(p0: Intent?): IBinder? {
        Log.e("TAG", "Inside onBind service")
        mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE)!! as BluetoothManager
        Log.e("TAG", "mBluetoothManager " + mBluetoothManager)
        if (mBluetoothManager != null) {
            mBluetoothAdapter = mBluetoothManager!!.adapter
            Log.e(TAG, "mBluetoothAdapter $mBluetoothAdapter")
        }
        return mBinder
    }


    override fun onUnbind(intent: Intent?): Boolean {
        Log.e("TAG", "Inside onUnbind service")
        return super.onUnbind(intent)
    }

    inner class LocalBinder : Binder() {
        fun getBleService(): BleService {
            return this@BleService
        }
    }

    //scan devices
    fun scanDevices(mBluetoothAdapter: BluetoothAdapter, mHandler: Handler) {
        Log.e("TAG", "Bluetooth adapter $mBluetoothAdapter")
        try {
            bgScanList = ArrayList()
            bgDeviceList = ArrayList()
            this.mHandler = mHandler
            if (mBluetoothAdapter.isEnabled) {
                bluetoothLeScanner = mBluetoothAdapter.bluetoothLeScanner
                /// mBluetoothAdapter.startDiscovery()
                Log.e("TAG", "is device discovering " + mBluetoothAdapter.isDiscovering)
                bluetoothLeScanner!!.startScan(mScanCallback)

                mHandler.postDelayed(
                        {
                            bluetoothLeScanner!!.stopScan(mScanCallback)
                        }, 5000)

            }
        } catch (e: Exception) {
            e.printStackTrace()
            bluetoothLeScanner!!.startScan(mScanCallback)
        }

        // Stops scanning after a pre-defined scan period.
//            mHandler.postDelayed({
//                bluetoothLeScanner!!.stopScan(mScanCallback)
//                if (bgScanList!!.isNotEmpty()) {
//                    val intent = Intent(ACTION_DEVICE_FOUND)
//                    Log.e("TAG", "Stop scan and send device list")
//                    intent.putParcelableArrayListExtra(DEVICE_LIST, bgScanList)
//                    LocalBroadcastManager.getInstance(this@BleService).sendBroadcast(intent)
//                } else { mHandler.postDelayed(
//                            {
//        broadcastUpdate(ACTION_RESTART_SCAN)
// }, 10000)
//   }
//
//            }, 3000)
//        }

    }


    private val mScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            Log.e("TAG", "onScanResult " + result.device.name)
            if (result.device.name.contains("ULI")) {
                bgScanList!!.add(result.device)
                bluetoothLeScanner!!.stopScan(this)
                val intent = Intent(ACTION_DEVICE_FOUND)
                Log.e("TAG", "Stop scan and send device list")
                intent.putParcelableArrayListExtra(DEVICE_LIST, bgScanList)
                LocalBroadcastManager.getInstance(this@BleService).sendBroadcast(intent)
            }
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)
            Log.e("TAG", "onBatchScanResults List $results")
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Log.e("TAG", "onScanFailed $errorCode")
            /* mHandler!!.postDelayed(
                          {
                              bluetoothLeScanner!!.startScan(this)
                          }, 3000)

        }*/
        }

    }
}