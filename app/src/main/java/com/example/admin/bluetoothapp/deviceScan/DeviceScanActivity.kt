package com.example.admin.bluetoothapp.deviceScan

import android.app.Activity
import android.app.ListActivity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.example.admin.bluetoothapp.R
import com.example.admin.bluetoothapp.REQUEST_ENABLE_BT
import com.example.admin.bluetoothapp.utility.AppBackgroundEvents
import kotlinx.android.synthetic.main.activity_device_scan.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.lang.Exception


private const val SCAN_PERIOD: Long = 10000

class DeviceScanActivity : ListActivity() {
    // private var mScanning: Boolean = false
    private var mLeDeviceListAdapter: LeDeviceListAdapter? = null
    private var scanDeviceList: MutableList<BluetoothDevice>? = null
    private var bluetoothLeScanner: BluetoothLeScanner? = null
    private val mBluetoothAdapter: BluetoothAdapter? by lazy(LazyThreadSafetyMode.NONE) {
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }


    private var mHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_scan)
        //toolbar setup
        //val baseToolbar: Toolbar = findViewById(R.id.scanToolbar)
        /*setSupportActionBar(baseToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        baseToolbar.setNavigationOnClickListener {
            finish()
        }*/

        try{
            mHandler = Handler()
            scanDeviceList = ArrayList()

            if (mBluetoothAdapter!!.isEnabled) {
                scanProgress.visibility = View.VISIBLE
                bluetoothLeScanner = mBluetoothAdapter!!.bluetoothLeScanner
                mLeDeviceListAdapter = LeDeviceListAdapter(this, scanDeviceList as ArrayList<BluetoothDevice>)
                listAdapter = mLeDeviceListAdapter
                list.onItemClickListener = mDeviceClickListener
                scanLeDevice(true)
            } else {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            }
        }
        catch (e:Exception)
        {
            Toast.makeText(this,"Exception Scanning "+e.message,Toast.LENGTH_SHORT).show();
        }

    }

    public override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
    }

    private val mLeScanCallback = object : ScanCallback() {

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)

            if(!result.device.name.isNullOrEmpty()&&result.device.name.contains(getString(R.string.uli_id)))
            {
                scanProgress.visibility = View.GONE
                Log.e("TAG", "onScanResult " + result.device.name)
                mLeDeviceListAdapter!!.addDevice(result.device)
                mLeDeviceListAdapter!!.notifyDataSetChanged()
            }
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)
            scanProgress.visibility = View.GONE
            Log.e("TAG", "onBatchScanResults List $results")
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            scanProgress.visibility = View.GONE
            Log.e("TAG", "onScanFailed $errorCode")
        }
    }

    private fun scanLeDevice(enable: Boolean) {
        try{
            mBluetoothAdapter!!.startDiscovery()
            if (enable) {
                bluetoothLeScanner!!.startScan(mLeScanCallback)
                // Stops scanning after a pre-defined scan period.
                mHandler!!.postDelayed({
                    scanProgress.visibility = View.GONE
                    //mScanning = false
                    if (mLeDeviceListAdapter!!.isListEmpty()) {
                        no_device_tv.visibility = View.VISIBLE
                    }
                    bluetoothLeScanner!!.stopScan(mLeScanCallback)
                }, SCAN_PERIOD)

            } else {
                scanProgress.visibility = View.GONE
                // mScanning = false
                bluetoothLeScanner!!.stopScan(mLeScanCallback)
            }
        }
        catch (e:Exception)
        {
         Toast.makeText(this,"Scan Exception "+e.message,Toast.LENGTH_SHORT).show()
        }


    }

    private val mDeviceClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->

        bluetoothLeScanner!!.stopScan(mLeScanCallback)
        val dataBundle = Bundle()
        dataBundle.putString(BluetoothDevice.EXTRA_DEVICE, scanDeviceList!![position].address)
        val result = Intent().putExtras(dataBundle)
        setResult(Activity.RESULT_OK, result)
        finish()
    }


    override fun onPause() {
        super.onPause()
        Log.e("TAG", "Inside Pause ")
        scanLeDevice(false)
        mLeDeviceListAdapter!!.clear()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
