package com.example.admin.bluetoothapp.utility

import com.example.admin.bluetoothapp.pojo.DoorStatusPojo

interface OnReceiveData {

    fun getDoorList(doorList:ArrayList<DoorStatusPojo>)
}