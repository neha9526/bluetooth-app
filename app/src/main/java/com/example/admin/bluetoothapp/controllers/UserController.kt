package com.example.admin.bluetoothapp.controllers


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatTextView
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bluelinelabs.conductor.Controller
import com.example.admin.bluetoothapp.DoorLockActivity
import com.example.admin.bluetoothapp.R
import com.example.admin.bluetoothapp.RegisteredUsers.RegisteredUserActivity
import com.example.admin.bluetoothapp.RegisteredUsers.UserListPojo
import com.example.admin.bluetoothapp.logDetails.LogDetailActivity
import com.example.admin.bluetoothapp.pojo.DoorStatusPojo
import com.example.admin.bluetoothapp.utility.*
import kotlinx.android.synthetic.main.layout_user_controller.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.time.Year
import java.util.*
import kotlin.collections.ArrayList

class UserController : Controller() , AdapterView.OnItemSelectedListener {

    var context: Context? = null
    var dataFrameList: ArrayList<ByteArray>? = null
    var mHandler: Handler? = null
    var isRemovingSelf: Boolean = false
    var doorLockActivity: DoorLockActivity? = null
    private var customCal: java.util.Calendar? = null
    private var hassleFreeData:String? = null
    var doorList: java.util.ArrayList<DoorStatusPojo>? = null
    var doorLockNameList: java.util.ArrayList<String>? = null
    var currentLockNo:String =""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
        context = applicationContext!!

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
        return inflater.inflate(R.layout.layout_user_controller, container, false)
    }

    override fun onAttach(view: View) {
        super.onAttach(view)
        mHandler = Handler()
        doorLockActivity = DoorLockActivity()

        doorList = java.util.ArrayList()
        doorList = parentController!!.args.getParcelableArrayList("DoorList")
        //setting up spinner for latch name
        if(doorList!!.isNotEmpty())
        {
            doorLockNameList = java.util.ArrayList()
            doorLockNameList!!.add("Select Lock")
            for(door in doorList!!)
            {
                doorLockNameList!!.add(door.lockName)
            }
        }
        if(doorLockNameList!!.isNotEmpty())
        {
            val expenseAdapter = ArrayAdapter(context, R.layout.textviewforspinner, doorLockNameList)
            view.latchNameChangeSpinner.adapter = expenseAdapter
            view.latchNameChangeSpinner.onItemSelectedListener = this
        }

        //initializing view
        if (Preferences.getHassleFreeMode(context!!)) {
            view.hassleFreeText.text = context!!.getString(R.string.dissable_hassle_free)
        }

        //hiding and showing hassle free layout
        if(doorList!!.size>1)
        {
            view.enableHasselFree.visibility = View.GONE
        }
        else{
            view.enableHasselFree.visibility = View.VISIBLE
        }

        //initializing calender and its variables
        customCal = java.util.Calendar.getInstance()
        val year = customCal!!.get(Calendar.YEAR)
        val month = customCal!!.get(Calendar.MONTH)
        val day = customCal!!.get(Calendar.DAY_OF_MONTH)

        //initializing date picker and its variables
        val hour = customCal!!.get(Calendar.HOUR)
        val minute = customCal!!.get(Calendar.MINUTE)
        val second = customCal!!.get(Calendar.SECOND)




        //Add normal user
        view.addNormalUser.setOnClickListener()
        { it ->
            val inflater = LayoutInflater.from(activity!!)
            val dialogView = inflater.inflate(R.layout.layout_adduser_popup, null)
            val toDate = dialogView.findViewById(R.id.tvToDate) as AppCompatTextView
            val toTime = dialogView.findViewById(R.id.tvToTime) as AppCompatTextView
            val btnSend = dialogView.findViewById(R.id.add_user_send) as AppCompatButton
            val fromDate = dialogView.findViewById(R.id.tvFromDate) as AppCompatTextView
            val fromTime = dialogView.findViewById(R.id.tvFromTime) as AppCompatTextView
            val selectTime = dialogView.findViewById(R.id.tvAccessiblePeriod) as AppCompatTextView
            val selectTimeLayout = dialogView.findViewById(R.id.layoutTimeSelection) as LinearLayout
            val mobileUser = dialogView.findViewById(R.id.normalUserMobileNo) as AppCompatEditText
            val yesRadioButton = dialogView.findViewById(R.id.hassleYes) as RadioButton
            val noRadioButton =  dialogView.findViewById(R.id.hassleNo) as RadioButton
            val dayAccess = dialogView.findViewById(R.id.dayAccess) as RadioButton
            val nightAccess = dialogView.findViewById(R.id.nightAccess) as RadioButton
            val dayNightAccess = dialogView.findViewById(R.id.dayNightAccess) as RadioButton

            val popUpBuilder = AlertDialog.Builder(activity!!)
            popUpBuilder.setView(dialogView)
            popUpBuilder.setCancelable(true)
            val alertDialog = popUpBuilder.create()
            alertDialog.show()

            selectTime.setOnClickListener()
            {
                selectTimeLayout.visibility = View.VISIBLE
            }
            //setting calender view for from date
            fromDate.setOnClickListener() {
                val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    fromDate.text = DateHelper.getYearMonthDateFormat(monthOfYear, year, dayOfMonth)
                }, year, month, day)
                dpd.datePicker.minDate = System.currentTimeMillis()
                dpd.show()
            }

            //setting time picker view
            fromTime.setOnClickListener()
            {
                TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener { p0, p1, p2 ->
                    val AM_PM = if (p1 < 12) {
                        "AM"
                    } else {
                        "PM"
                    }

                    fromTime.text = DateHelper.getHourMinSec(p1, p2) + " " + AM_PM
                }, hour, minute, false).show()

            }

            //setting up date picker for to date
            toDate.setOnClickListener() {
                if (fromDate.text.isNotEmpty() && fromTime.text.isNotEmpty()) {
                    val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        // Display Selected date in textbox
                        toDate.text = DateHelper.getYearMonthDateFormat(monthOfYear, year, dayOfMonth)
                    }, year, month, day)
                    dpd.datePicker.minDate = DateHelper.getLongDate(fromDate.text.toString() + " " + fromTime.text.toString())
                    dpd.show()
                } else {
                    Toast.makeText(context, "Select from date first", Toast.LENGTH_SHORT).show()
                }
            }
            //setting up time picker for to date
            toTime.setOnClickListener()
            {
                TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener { p0, p1, p2 ->
                    val AM_PM = if (p1 < 12) {
                        "AM"
                    } else {
                        "PM"
                    }
                    toTime.text = DateHelper.getHourMinSec(p1, p2) + " " + AM_PM
                }, hour, minute, false).show()

            }




            //send button logic
            btnSend.setOnClickListener() {
                if (mobileUser.text!!.isNotEmpty()) {
                    var flag = true
                    dataFrameList = ArrayList()
                    val key = context!!.getString(R.string.AddNormalKey)
                    val mobCommand = key + mobileUser.text.toString().toLong().toString(16)
                    dataFrameList!!.add(mobCommand.toByteArray(Charset.defaultCharset()))

                    if (fromDate.text.isNotEmpty()) {
                        if (fromTime.text.isNotEmpty()) {
                            val timeStamp = DateHelper.getLongDate(fromDate.text.toString() + " "
                                    + fromTime.text.toString())
                            dataFrameList!!.add((key + timeStamp).toByteArray(Charset.defaultCharset()))
                        } else {
                            flag = false
                            fromTime.error = "Please select time"
                        }
                    } else {
                        dataFrameList!!.add(key.toByteArray(Charset.defaultCharset()))
                    }

                    if (toDate.text.isNotEmpty()) {
                        if (toTime.text.isNotEmpty()) {
                            val timeStamp = DateHelper.getLongDate(toDate.text.toString() + " " +
                                    toTime.text.toString())
                            dataFrameList!!.add((key + timeStamp).toByteArray(Charset.defaultCharset()))
                        } else {
                            flag = false
                            toTime.error = "Please select time"
                        }
                    } else {
                        dataFrameList!!.add(key.toByteArray(Charset.defaultCharset()))
                    }

                    //adding check for hassle free mode and day and nigh access
                    if(yesRadioButton.isChecked)
                    {
                        when{
                            dayAccess.isChecked->{
                                hassleFreeData = context!!.getString(R.string.AddNormalKey)+"0011"
                            }
                            nightAccess.isChecked ->{
                                hassleFreeData = context!!.getString(R.string.AddNormalKey)+"0021"
                            }
                            dayNightAccess.isChecked ->{
                                hassleFreeData = context!!.getString(R.string.AddNormalKey)+"0001"
                            }

                        }
                        dataFrameList!!.add(hassleFreeData!!.toByteArray(Charset.defaultCharset()))
                    }
                    else if(noRadioButton.isChecked){
                        when{
                            dayAccess.isChecked->{
                                hassleFreeData = context!!.getString(R.string.AddNormalKey)+"0010"
                            }
                            nightAccess.isChecked ->{
                                hassleFreeData = context!!.getString(R.string.AddNormalKey)+"0020"
                            }
                            dayNightAccess.isChecked ->{
                                hassleFreeData = context!!.getString(R.string.AddNormalKey)+"0000"
                            }

                        }
                        dataFrameList!!.add(hassleFreeData!!.toByteArray(Charset.defaultCharset()))
                    }
                    else{
                        Toast.makeText(context, "Please select hassle free access", Toast.LENGTH_SHORT).show()
                        return@setOnClickListener
                    }


                    if (flag) {
                        EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                        alertDialog.dismiss()
                    }
                } else {
                    Toast.makeText(context, "Enter mobile number", Toast.LENGTH_SHORT).show()
                }
            }
        }

        //Add master user
        view.addMasterUser.setOnClickListener()
        {
            addUser(context!!.getString(R.string.add_master_user), context!!.getString(R.string.AddMasterKey))
        }
        //remove user
        view.removeUser.setOnClickListener()
        {
            addUser(context!!.getString(R.string.remove_user), context!!.getString(R.string.removeUserkey))
        }
        //remove self
        view.removeSelf.setOnClickListener()
        {
            val builder = AlertDialog.Builder(activity!!)
            builder.setMessage("Do you want to remove yourself?")
                    .setPositiveButton("Yes") { dialog, id ->
                        dataFrameList = ArrayList()
                        isRemovingSelf = true
                        val hexCommand = context!!.getString(R.string.removeUserkey) + Preferences.getMobileNo(context!!)
                        Log.e("TAG", "Hex command " + hexCommand)
                        dataFrameList!!.add(hexCommand.toByteArray(Charset.defaultCharset()))
                        EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                    }.setNegativeButton("Cancel"
                    ) { dialog, id ->
                        dialog.dismiss()
                    }.show()

        }
        //see logs
        view.seeLogs.setOnClickListener()
        {
            view.logProgress.visibility = View.VISIBLE
            mHandler!!.postDelayed({
                view.logProgress.visibility = View.GONE
            }, 4000)
            dataFrameList = ArrayList()
            val currentTime = System.currentTimeMillis()
            val hexData = context!!.getString(R.string.log_details_key) + currentTime.toString(16)
            dataFrameList!!.add(hexData.toByteArray(Charset.defaultCharset()))
            EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
        }

        //change lock name
        view.changeLockName.setOnClickListener {
            addUser(context!!.getString(R.string.change_lock_name_comm), context!!.getString(R.string.change_lock_name_key))
        }
        //Hassle free mode
        view.enableHasselFree.setOnClickListener()
        {
            val builder = AlertDialog.Builder(activity!!)
            dataFrameList = ArrayList()
            //enabling hassle free mode
            if (!Preferences.getHassleFreeMode(context!!)) {
                builder.setMessage("Do you want to turn on hassle free mode?")
                        .setPositiveButton("Enable") { dialog, id ->
                            val hassleFreeData =context!!.getString(R.string.hasslefree_key)+context!!.getString(R.string.hassle_free_on)
                            Log.e("TAG","Hassle Free Data "+hassleFreeData)
                            dataFrameList = ArrayList()
                            dataFrameList!!.add(hassleFreeData.toByteArray(Charset.defaultCharset()))
                            EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                            Preferences.setHassleFreeMode(context!!, true)

                        }.setNegativeButton("Cancel") { dialog, id ->
                            dialog.dismiss()
                        }
            }
            //disabling hassle free mode
            else {
                builder.setMessage("Do you want to turn off hassle free mode?")
                        .setPositiveButton("Disable") { dialog, id ->
                            val hassleFreeData =context!!.getString(R.string.hasslefree_key)+context!!.getString(R.string.hassle_free_off)
                            Log.e("TAG","Hassle Free Data "+hassleFreeData)
                            dataFrameList = ArrayList()
                            dataFrameList!!.add(hassleFreeData.toByteArray(Charset.defaultCharset()))
                            Log.e("TAG","Hassle Free command  "+hassleFreeData)
                            EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                            Preferences.setHassleFreeMode(context!!, false)
                        }.setNegativeButton("Cancel"
                        ) { dialog, id ->
                            dialog.dismiss()
                        }
            }

            // Create the AlertDialog object and return it
            builder.create().show()
        }

        //onclick listener of registered user
        view.registeredUser.setOnClickListener()
        {
            view.logProgress.visibility = View.VISIBLE
            mHandler!!.postDelayed({
                view.logProgress.visibility = View.GONE
            }, 3000)
            dataFrameList = ArrayList()
            val hexData =context!!.getString(R.string.get_users_key)
            dataFrameList!!.add(hexData.toByteArray(Charset.defaultCharset()))
            EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
        }
    }

    //listen event of adding users
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun getEventResponse(allResponseEvent: AllResponseEvent) {
        when (allResponseEvent.response) {
            context!!.getString(R.string.added_master) -> {
                Toast.makeText(applicationContext, "Master Added Successfully", Toast.LENGTH_SHORT).show()
            }
            context!!.getString(R.string.added_normal_user) -> {
                Toast.makeText(applicationContext, "User Added Successfully", Toast.LENGTH_SHORT).show()
            }
            context!!.getString(R.string.removed_normal) -> {
                Toast.makeText(applicationContext, "Removed User Successfully", Toast.LENGTH_SHORT).show()
            }
            context!!.getString(R.string.removed_mster) -> {
                when (isRemovingSelf) {
                    true -> {
                        allResponseEvent.mService.disconnect()
                    }
                    else -> {
                        Toast.makeText(applicationContext, "Removed Master Successfully", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            context!!.getString(R.string.lock_name_change) -> {
                Toast.makeText(applicationContext, "Name Updated Successfully", Toast.LENGTH_SHORT).show()
            }
            context!!.getString(R.string.lock_name_change_error) -> {
                Toast.makeText(applicationContext, "Cannot Change Lock Name", Toast.LENGTH_SHORT).show()
            }
            //for duplicate users and if user master list is full
            "DU USR" ->{
                Toast.makeText(applicationContext, "User already exist", Toast.LENGTH_SHORT).show()
            }
            "M FULL" ->{
                Toast.makeText(applicationContext, "Can not add more master", Toast.LENGTH_SHORT).show()
            }
            "U FULL" ->{
                Toast.makeText(applicationContext, "Can not add more user", Toast.LENGTH_SHORT).show()
            }
        }
    }

    //listen even of Log details
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun getLogDetails(logDetailsEvent: LogDetailsEvent) {
        view!!.logProgress.visibility = View.GONE
        if (logDetailsEvent.logList.isNotEmpty()) {
            Toast.makeText(applicationContext, "Logs Received successfully", Toast.LENGTH_SHORT).show()
            val intent = Intent(context, LogDetailActivity::class.java)
           // intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            val dataBundle = Bundle()
            dataBundle.putParcelableArrayList("Logs", logDetailsEvent.logList)
            intent.putExtras(dataBundle)
            startActivity(intent)
        } else {
            Toast.makeText(applicationContext, "No logs available", Toast.LENGTH_SHORT).show()
        }
    }

    //Subscribe to listen userList
    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    fun getUserList(userList: UserListPojo) {
        if (userList.userList.isNotEmpty()) {
            Toast.makeText(applicationContext, "User list updated successfully", Toast.LENGTH_SHORT).show()
            val intent = Intent(context, RegisteredUserActivity::class.java)
            /*intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT*/
            val dataBundle = Bundle()
            dataBundle.putStringArrayList("Users", userList.userList)
            intent.putExtras(dataBundle)
            startActivity(intent)
            view!!.logProgress.visibility = View.GONE
        } else {
            Toast.makeText(applicationContext, "No users available", Toast.LENGTH_SHORT).show()
        }
    }


    private fun addUser(command: String, key: String) {
        val inflater = LayoutInflater.from(activity!!)
        val dialogView = inflater.inflate(R.layout.user_popup, null)
        val alertTitle = dialogView.findViewById(R.id.commandName) as AppCompatTextView
        val userData = dialogView.findViewById(R.id.commandData) as AppCompatEditText
        val btnSend = dialogView.findViewById(R.id.send_user) as AppCompatButton
        alertTitle.text = command

        when(key){
            context!!.getString(R.string.change_lock_name_key) ->{
                userData.inputType = InputType.TYPE_CLASS_TEXT
                userData.hint = "Enter lock name"
                userData.setText(Preferences.getLockName(context!!))
            }
            context!!.getString(R.string.change_latch_name) ->{
                userData.inputType = InputType.TYPE_CLASS_TEXT
                userData.hint = "Enter latch name"
                userData.setText( view!!.latchNameChangeSpinner.selectedItem.toString())
            }

        }
        val popUpBuilder = AlertDialog.Builder(activity!!)
        popUpBuilder.setView(dialogView)
        popUpBuilder.setCancelable(true)
        val alertDialog = popUpBuilder.create()
        alertDialog.show()

        btnSend.setOnClickListener {
            dataFrameList = ArrayList()
            if (userData.text.toString().isNotEmpty()) {
                alertDialog.dismiss()
                when(key)
                {
                    context!!.getString(R.string.change_lock_name_key) ->{
                        val hexCommand = key + userData.text.toString()
                        dataFrameList!!.add(hexCommand.toByteArray(Charset.defaultCharset()))
                        EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                    }
                    context!!.getString(R.string.change_latch_name) ->{
                        val hexCommand = key+currentLockNo+userData.text.toString()
                        Log.e("TAG","Latch name hex "+hexCommand)
                        dataFrameList!!.add(hexCommand.toByteArray(Charset.defaultCharset()))
                        EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                    }
                    else ->{
                        val hexCommand = key + userData.text.toString().toLong().toString(16)
                        dataFrameList!!.add(hexCommand.toByteArray(Charset.defaultCharset()))
                        EventBus.getDefault().post(WriteDataEvent(dataFrameList!!))
                    }
                }

            } else {
                when (key) {
                    context!!.getString(R.string.change_lock_name_key) -> {
                        Toast.makeText(context, context!!.getString(R.string.enter_lock_name), Toast.LENGTH_SHORT).show()
                    }
                    context!!.getString(R.string.change_latch_name) ->
                    {
                        Toast.makeText(context, "Enter the latch name", Toast.LENGTH_SHORT).show()
                    }
                    else -> {
                        Toast.makeText(context, context!!.getString(R.string.enter_mob_no), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if(position>0)
        {
            currentLockNo = doorList!![position-1].lockNumber
            addUser("Change Latch Name","40")
        }

    }

}
